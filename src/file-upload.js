import React from 'react'
import Header from './header'

function FileUpload() {
  const vRef = React.useRef(null)
  const [file, fileSet] = React.useState(null)

  const handleFileChange = e => {
    fileSet(e.target.files[0])
  }

  React.useEffect(() => {
    const addVideo = () => {
      vRef.current.src = URL.createObjectURL(file)
    }
    if (vRef.current && file !== null) {
      addVideo()
    }
  }, [file])

  console.log('🚀 ~ file', file)

  return (
    <div>
      <Header />
      <input type="file" accept="video/*" capture onChange={handleFileChange} />

      <div>
        {file ? (
          <video src={file} controls ref={vRef} autoPlay playsInline></video>
        ) : null}
      </div>
    </div>
  )
}

export default FileUpload

import { Route, Routes } from 'react-router-dom'
import FileUpload from './file-upload'
import Webcams from './react-record'
import ReactWebcam from './react-webcam'

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<ReactWebcam />} />
        <Route path="react-record-webcam" element={<Webcams />} />
        <Route path="file-upload" element={<FileUpload />} />
      </Routes>
    </div>
  )
}

export default App

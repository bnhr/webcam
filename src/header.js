import { Link } from 'react-router-dom'

function Header() {
  return (
    <div className="nav">
      <Link to="/">react-webcam</Link>
      <Link to="/react-record-webcam">react-record-webcam</Link>
      <Link to="/file-upload">file-upload</Link>
    </div>
  )
}

export default Header
